# Déscription

Functions est une librairie utilitaire de classe PHP 

## Installation

## With composer 
```php
    composer require fabacks/functions
```

### Without composer
```php
    require_once YOUR_PATH."Functions/Autoloader.php";
    Functions_Autoloader::register();
```

## Usage
```php
use Fabacks\Functions;

\Functions\Tests::isMail( $_['mail'] );
\Functions\Files::folder_sizeReadble(__DIR__);
```


## License
[MIT](https://choosealicense.com/licenses/mit/)