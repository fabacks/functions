<?php

namespace Fabacks\Functions\Tests\Jsons;
use Fabacks\Functions\Jsons;

/**
 * Class JsonsTest
 *
 */
class JsonsTest extends \PHPUnit\Framework\TestCase {
    
    public function test_isValid() {
        // Json valide
        $json = '[{"user_id":13,"username":"stack"},{"user_id":14,"username":"over"}]';
        $this->assertTrue( Jsons::isValid($json) );

        // Json invalide
        $json = '{background-color:yellow;color:#000;padding:10px;width:650px;}';
        $this->assertFalse( Jsons::isValid($json) );
    }  

    public function test_validate() {
        // Json valide
        $json = '[{"user_id":13,"username":"stack"},{"user_id":14,"username":"over"}]';
        $output = Jsons::validate($json);
        $this->assertEquals($output, true);

        // Json invalide
        $json = '{background-color:yellow;color:#000;padding:10px;width:650px;}';
        $output = Jsons::validate($json);
        $this->assertEquals($output, 'Syntax error, malformed JSON.');
    }   

}