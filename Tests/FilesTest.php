<?php

namespace Fabacks\Functions\Tests\Files;
use Fabacks\Functions\Files;

/**
 * Class FilesTest
 *
 */
class FilesTest extends \PHPUnit\Framework\TestCase
{
    function test_createFileName()
    {
        $str = "Le * PHP * C\'est magique          !!";
        $rtn = Files::createFileName($str, 50);
        $this->assertEquals($rtn, "Le - PHP - C-'est magique --");

        $str = "Le text est très très long haha !!";
        $rtn = Files::createFileName($str, 5);
        $this->assertEquals($rtn, "Le te");

        $str = "Avec des accents éèà";
        $rtn = Files::createFileName($str, 50);
        $this->assertEquals($rtn, "Avec des accents eea");

        $str = "L’accompagnement conformité (test)";
        $rtn = Files::createFileName($str, 50);
        $this->assertEquals($rtn, "L'accompagnement conformite (test)");
        
    }

    public function test_folder_size()
    {
        $size = Files::folder_size(__DIR__);
        $this->assertTrue($size > 100);
    }   

    public function test_folder_sizeReadble()
    {
        // On test si le suffix Mo est présent dans la conversion
        $size = Files::folder_sizeReadble(__DIR__, 'M', true);
        $this->assertStringEndsWith("Mo", (string)$size);

        // On test si le dossier de test est inferieure à 3Mo
        $size = Files::folder_sizeReadble(__DIR__, 'M');
        $this->assertTrue($size < 3);
    }   

  

    
}