<?php

namespace Fabacks\Functions\Tests\IPs;
use Fabacks\Functions\IPs;

/**
 * Class IPsTest
 *
 */
class IPsTest extends \PHPUnit\Framework\TestCase {
    
    public function test_isIP() {
        $ip = "192.168.1.22"; 
        $this->assertTrue( IPs::isIp($ip) );

        $ip = "2001:0db8:0000:85a3:0000:0000:ac1f:8001"; 
        $this->assertTrue( IPs::isIp($ip) );

        $ip = "192.168.1.350"; 
        $this->assertFalse( IPs::isIp($ip) );
    }   

    public function test_isLocalhost() {
        $this->assertTrue( IPs::isLocalhost() );

        $url = "127.0.0.1";
        $this->assertTrue( IPs::isLocalhost($url) );

        $url = "172.0.0.2";
        $this->assertFalse( IPs::isLocalhost($url) );
    } 

}