<?php

namespace Fabacks\Functions\Tests\Dates;
use Fabacks\Functions\Dates;

/**
 * Class DatesTest
 *
 */
class DatesTest extends \PHPUnit\Framework\TestCase 
{
    
    // public function test_isDate() {
    //     $this->assertEquals(Dates::isDate("05/06/2019", "DMY"), true);
    //     $this->assertEquals(Dates::isDate("12-30-2019", "MDY"), true);
    //     $this->assertEquals(Dates::isDate("2019-05-30", "YMD"), true);
    //     $this->assertEquals(Dates::isDate("2019-30-30", "YMD"), false);
    // }   

    // public function test_timestampToDate() {
    //     $this->assertEquals(Dates::timestampToDate("1565077546"), '06 Août 2019 à 07:45:46');
    // } 

    // public function test_toTime() {
    //     $this->assertEquals(Dates::toTime("06/08/2019"), 1565049600);
    //     $this->assertEquals(Dates::toTime("06/08/2019 09:50:00"), 1565049600);
    //     $this->assertEquals(Dates::toTime("06/08/2019 09:50:00", true), 1565085000);
    // }  

    // public function test_leapYear() {
    //     $this->assertEquals(Dates::isLeapYear(2016), true);
    //     $this->assertEquals(Dates::isLeapYear(2017), false);
    //     $this->assertEquals(Dates::isLeapYear(2020), true);
    // }  

    
    // public function test_getMonthName() {
    //     $this->assertEquals(Dates::getMonthName(1), 'Jan');
    //     $this->assertEquals(Dates::getMonthName(4), 'Avr');
    //     $this->assertEquals(Dates::getMonthName(13), '');
    // } 

    // public function test_getDayName() {
    //     $this->assertEquals(Dates::getDayName("Mon"), 'L');
    //     $this->assertEquals(Dates::getDayName("Sat"), 'S');
    //     $this->assertEquals(Dates::getDayName("wx"), '');
    // }     

    // public function test_getFullDayName() {
    //     $this->assertEquals(Dates::getFullDayName("Mon"), 'Lundi');
    //     $this->assertEquals(Dates::getFullDayName("Thu"), 'Jeudi');
    //     $this->assertEquals(Dates::getFullDayName("aa"), '');
    // }     

    public function test_getNumberDayName() 
    {
        $this->assertEquals(Dates::getNumberDayName(1), 'Lundi');
        $this->assertEquals(Dates::getNumberDayName(4), 'Jeudi');
        $this->assertEquals(Dates::getNumberDayName(13), '');
    }





    public function test_number_of_weeks_in_year() 
    {
        $this->assertEquals(Dates::number_of_weeks_in_year(2021), 52);
        $this->assertEquals(Dates::number_of_weeks_in_year(2022), 52);
        $this->assertEquals(Dates::number_of_weeks_in_year(2023), 52);
        $this->assertEquals(Dates::number_of_weeks_in_year(2026), 53);
    } 

    public function test_get_month_from_week_number() 
    {
        $this->assertEquals(Dates::get_month_from_week_number(1, 2021), 1);
        $this->assertEquals(Dates::get_month_from_week_number(22, 2023), 5);
        $this->assertEquals(Dates::get_month_from_week_number(50, 2021), 12);
        $this->assertEquals(Dates::get_month_from_week_number(53, 2026), 12);
    }

    // public function display_week_days() 
    // {
    // }



    public function test_convert_time_to_secondes()
    {
        $resp = Dates::convert_time_to_secondes('01:00:00');
        $this->assertEquals($resp, 3600);

        $resp = Dates::convert_time_to_secondes('01:00');
        $this->assertEquals($resp, 3600);

        $resp = Dates::convert_time_to_secondes('01:01:00');
        $this->assertEquals($resp, 3660);

        $resp = Dates::convert_time_to_secondes('01:00:01');
        $this->assertEquals($resp, 3601);

        $resp = Dates::convert_time_to_secondes('05:59:59');
        $this->assertEquals($resp, 21599);

        $resp = Dates::convert_time_to_secondes('85:00');
        $this->assertEquals($resp, 306000);
    }



    public function test_calculate_time_difference()
    {
        $resp = Dates::calculate_time_difference('02:00', '01:00');
        $this->assertEquals($resp, '01:00:00');

        $resp = Dates::calculate_time_difference('01:00', '23:00');
        $this->assertEquals($resp, '-22:00:00');

        $resp = Dates::calculate_time_difference('01:00', '29:00');
        $this->assertEquals($resp, '-28:00:00');
    }

    public function test_calculate_time_add()
    {
        $resp = Dates::calculate_time_add('01:01:01', '01:01:01');
        $this->assertEquals($resp, '02:02:02');
    }

    public function test_calculate_time_subtract()
    {
        $resp = Dates::calculate_time_subtract('01:01:01', '01:01:01');
        $this->assertEquals($resp, '00:00:00');

        $resp = Dates::calculate_time_subtract('02:01:01', '01:01:01');
        $this->assertEquals($resp, '01:00:00');

        $resp = Dates::calculate_time_subtract('01:01:01', '02:01:01');
        $this->assertEquals($resp, '-01:00:00');
    }

    public function test_calculate_sum_time()
    {
        $sum = array('03:00', '-01:00', '00:30:01');
        $resp = Dates::calculate_sum_times($sum);
        $this->assertEquals($resp, '02:30:01');
    }

}