<?php

namespace Fabacks\Functions\Tests\Urls;
use Fabacks\Functions\Urls;

/**
 * Class UrlsTest
 *
 */
class UrlsTest extends \PHPUnit\Framework\TestCase
{
    
    public function test_isUrl() 
    {
        $url = "http://test.fr";
        $this->assertTrue( Urls::isUrl($url) );

        $url = "sdfsdf";
        $this->assertFalse( Urls::isUrl($url) );
    }

    public function test_getValueFromUrl() 
    {
        $url  = "http://test.fr?test=1";
        $resp = Urls::getValueFromUrl($url, 'test');
        $this->assertEquals($resp, '1');

        $url = "http://test.fr?name=Une souris verte";
        $resp = Urls::getValueFromUrl($url, 'name');
        $this->assertEquals($resp, 'Une souris verte');

        $url = "http://test.fr?param=fails";
        $resp = Urls::getValueFromUrl($url, 'name');
        $this->assertEquals($resp, '');
    } 

    // public function test_getUrl() 
    // {
    //     $url  = "local.test/index.html";
    //     $resp = Urls::getUrl();
    //     $this->assertEquals($resp, $url);
    // } 

    // public function test_getPageUrl() 
    // {
    //     $resp = Urls::getPageUrl();
    //     $this->assertEquals($resp, 'index.html');

    //     $url  = "local.test/action.html?test=a";
    //     $resp = Urls::getPageUrl($url);
    //     $this->assertEquals($resp, 'action.html');
    // }

}