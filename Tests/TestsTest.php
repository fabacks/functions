<?php

namespace Fabacks\Functions\Tests\Tests;
use Fabacks\Functions\Tests;

/**
 * Class TestsTest
 *
 */
class TestsTest extends \PHPUnit\Framework\TestCase {
    
    public function test_isPhpCli() {
        $this->assertTrue( Tests::isPhpCli() );
    }

    public function test_isMail() {
        $test = "jean";
        $this->assertFalse( Tests::isMail($test) );
        
        $test = "jean@test.fr";
        $this->assertTrue( Tests::isMail($test) );
    }

    public function test_isPhone() {
        $test = "0680903945";
        $this->assertTrue( Tests::isPhone($test) );

        $test = "068090394";
        $this->assertFalse( Tests::isPhone($test) );

        $test = "+33068090394";
        $this->assertTrue( Tests::isPhone($test) );

        $test = "(+91) 444-444-5555";
        $this->assertTrue( Tests::isPhone($test) );
    }

    public function test_isTrue() {
        $arrayTest = array(1, '1', 'true', true);
        foreach($arrayTest as $test): 
            $resp = Tests::isTrue($test);
            $this->assertTrue($resp);
        endforeach;

        $arrayTest = array(0, '0', 'false', false, 'a', 'blabla');
        foreach($arrayTest as $test): 
            $resp = Tests::isTrue($test);
            $this->assertFalse($resp);
        endforeach;

        $arrayTest = array('a', 'blabla');
        foreach($arrayTest as $test): 
            $resp = Tests::isTrue($test, true);
            $this->assertNull($resp);
        endforeach;
    }


    // public function test_isSiren()
    // {
    //     // Correcte
    //     $list = array("210300018, 803970128");
    //     foreach($list as $item):
    //         $this->assertTrue( Tests::isSiren($item) );
    //     endforeach;

    //     // Faux
    //     $list = array("210300018157892", "sdqd5712dsq", '803970126');
    //     foreach($list as $item):
    //         $this->assertFalse( Tests::isSiren($item) );
    //     endforeach;
    // }

    // public function test_isSiret()
    // {
    //     // Correcte
    //     $list = array("80397012800010");
    //     foreach($list as $item):
    //         $this->assertTrue( Tests::isSiren($item) );
    //     endforeach;

    //     // Faux
    //     $list = array("80397012804010", "sdqd5712dsq");
    //     foreach($list as $item):
    //         $this->assertFalse( Tests::isSiren($item) );
    //     endforeach;
    // }

    public function test_isMultiple() 
    {
        $resp = Tests::isMultiple(12, 3);
        $this->assertTrue($resp);

        $resp = Tests::isMultiple(12, 4);
        $this->assertTrue($resp);

        $resp = Tests::isMultiple(98, 49);
        $this->assertTrue($resp);

        $resp = Tests::isMultiple(10, 3);
        $this->assertFalse($resp);

        $resp = Tests::isMultiple(49, 2);
        $this->assertFalse($resp);
    }
}