<?php

namespace Fabacks\Functions\Tests\Units;
use Fabacks\Functions\Units;

/**
 * Class UnitsTest
 *
 */
class UnitsTest extends \PHPUnit\Framework\TestCase {
    
    public function test_temperature() {
        $this->assertEquals(Units::temperature(5, 'C', 'C'), 5);
        $this->assertEquals(Units::temperature(5, 'C', 'C', 2), 5);

        $this->assertEquals(Units::temperature(50, 'C', 'C', 2), 50);
        $this->assertEquals(Units::temperature(50, 'C', 'F', 2), 122);
        $this->assertEquals(Units::temperature(50, 'C', 'K', 2), 323.15);
        $this->assertEquals(Units::temperature(50, 'C', 'Re', 2), 40);
        $this->assertEquals(Units::temperature(50, 'C', 'Ra', 2), 581.67);

        $this->assertEquals(Units::temperature(50, 'F', 'C', 2), 10);
        $this->assertEquals(Units::temperature(50, 'F', 'F', 2), 50);
        $this->assertEquals(Units::temperature(50, 'F', 'K', 2), 283.15);
        $this->assertEquals(Units::temperature(50, 'F', 'Re', 2), 8);
        $this->assertEquals(Units::temperature(50, 'F', 'Ra', 2), 509.67);

        $this->assertEquals(Units::temperature(50, 'K', 'C', 2), -223.15);
        $this->assertEquals(Units::temperature(50, 'K', 'F', 2), -369.67);
        $this->assertEquals(Units::temperature(50, 'K', 'K', 2), 50);
        $this->assertEquals(Units::temperature(50, 'K', 'Re', 2), -178.52);
        $this->assertEquals(Units::temperature(50, 'K', 'Ra', 2), 90);

    }   

    public function test_speed() {
        $this->assertEquals(Units::speed(1, "MPH", "MPH", 2), 1);
        $this->assertEquals(Units::speed(1, "MPH", "KM/H"), 1.609344);
    } 


}