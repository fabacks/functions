<?php

namespace Fabacks\Functions\Tests\Colors;
use Fabacks\Functions\Colors;

/**
 * Class ColorsTest
 *
 */
class ColorsTest extends \PHPUnit\Framework\TestCase {
    
    public function test_isColorHexa() {
        $this->assertEquals(Colors::isColorHexa("#f5f5f5"), true);
        $this->assertEquals(Colors::isColorHexa("f5f5f5"), false);
        $this->assertEquals(Colors::isColorHexa("#zzz"), false);
    }

    public function test_hexaToRGB() {
        $this->assertEquals(Colors::hexaToRGB("#FFFFFF"), array(255,255,255) );
        $this->assertEquals(Colors::hexaToRGB("#f5f5f5"), array(245,245,245) );
        $this->assertEquals(Colors::hexaToRGB("#zzz"), array() );
    }

    public function test_getLight() {
        $light = Colors::getLight("#FFFFFF", 510.0);
        $this->assertEquals($light, 1);
        
        $light = Colors::getLight("#000000", 510.0);
        $this->assertEquals($light, 0);

        $light = Colors::getLight("#f5f5f5", 510.0);
        $this->assertEquals($light, 0.9607843137254902);

        $light = Colors::getLight("#x", 510.0);
        $this->assertEquals($light, 0.5);
    }
    
}