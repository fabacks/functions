<?php

namespace Fabacks\Functions\Tests\Geos;
use Fabacks\Functions\Geos;

/**
 * Class FilesTest
 *
 */
class GeosTest extends \PHPUnit\Framework\TestCase 
{

    public function test_distance() 
    {
        // Lyon : Latitude : 45.750000 || Longitude : 4.850000
        // Paris : Latitude : 48.866667  || Longitude : 2.333333
        $dist = Geos::distance(45.750000, 4.850000, 48.866667, 2.333333, 'K') ;
        $this->assertEquals($dist, 395.02863890956223);

        $dist = Geos::distance(45.750000, 4.850000, 48.866667, 2.333333, 'N') ;
        $this->assertEquals($dist, 213.1569571384762);

        $dist = Geos::distance(45.750000, 4.850000, 48.866667, 2.333333, 'M') ;
        $this->assertEquals($dist, 245.45941632712598);
    }

}