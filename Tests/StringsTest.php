<?php

namespace Fabacks\Functions\Tests\Strings;
use Fabacks\Functions\Strings;

/**
 * Class StringsTest
 *
 */
class StringsTest extends \PHPUnit\Framework\TestCase {
    
    public function test_implode()
    {
        $test = array('t', 'a', 'b');
        $this->assertEquals(Strings::implode('.', $test), "t.a.b");

        $test = array('t', 'e', 's', 't');
        $this->assertEquals(Strings::implode('', $test, 1), "est");

        $test = array('t', 'e', 's', 't');
        $this->assertEquals(Strings::implode('', $test, 1, 2), "es");
    }

    public function test_capitaliseName()
    {
        $test = "jean";
        $this->assertEquals(Strings::capitaliseName($test), "Jean");
        
        $test = "jean-marie";
        $this->assertEquals(Strings::capitaliseName($test), "Jean-Marie");

        $test = "jean-françois";
        $this->assertEquals(Strings::capitaliseName($test), "Jean-François");

        $test = "pierre le b'houd";
        $this->assertEquals(Strings::capitaliseName($test), "Pierre le b'houd");
    }

    public function test_checkPassword()
    {
        $lenghtMin = 8;

        $test = "az";
        // $this->assertTrue( Strings::checkPassword($test, $lenghtMin, true, false, false, false) );

        $test = "azertyuio";
        $this->assertTrue( Strings::checkPassword($test, $lenghtMin, true, false, false) );
        
        $test = "AZERTYUIO";
        $this->assertTrue( Strings::checkPassword($test, $lenghtMin, false, true, false) );

        $test = "123456789";
        $this->assertTrue( Strings::checkPassword($test, $lenghtMin, false, false, true) );
    }

    public function test_empty() 
    {
        $string = "   ";
        $this->assertTrue( Strings::isEmpty($string) );

        $string = null;
        $this->assertTrue( Strings::isEmpty($string) );

        $string = 0;
        $this->assertFalse( Strings::isEmpty($string) );

        $string = "    aaa      ";
        $this->assertFalse( Strings::isEmpty($string) );

        $string = "    dsfdsfsdf      f";
        $this->assertFalse( Strings::isEmpty($string) );

        $string = "aaa";
        $this->assertFalse( Strings::isEmpty($string) );
    }

}