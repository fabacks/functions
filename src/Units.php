<?php
/**
*	@class \Fabacks\Functions\Units
*	@description Classe utilitaire de conversion
*	@author Fabien COLAS
*	@site  dahoo.Fr
*	@gith https://github.com/Fabacks
*	@Copyright Licence CC-by-nc-sa 
*	@Update : 21/08/2019
*	@version 1.0.0
*/
namespace Fabacks\Functions;

class Units
{

    /**
    * Converstion de temperature.
    * Unité de conversion disponible : F => Fahrenheit, C => Celsius, K => Kelvin, Re => Reaumur, Ra => Rankine 
    *
    * @param float $value Valeur
    * @param string $in  Unité d'entrée
    * @param string $out Unité de sortie
    * @param integer $round
    * @return float
    */
    public static function temperature(float $value, string $in, string $out, int $round = null)
    {
        $convert = 0;
        $in = \strtoupper($in);
        $out = \strtoupper($out);
        $tab_val = array("C", "F", "K", "RE", "RA");      
        if ( !(in_array($in, $tab_val) || !in_array($out, $tab_val))) 
            return 0; 

        switch ($in) { 
            case "C" : // degres Celsius 
                switch ($out) { 
                    case "C" : 
                        $convert = $value; 
                    break;
                    case "F" : 
                        $convert = ($value * 9 / 5) + 32; 
                    break;
                    case "K" : 
                        $convert = ($value + 273.15); 
                    break;
                    case "RE" : 
                        $convert = ($value * 4 / 5); 
                    break;
                    case "RA" : 
                        $convert = (($value + 273.15) * 1.8); 
                    break;
                } 
            break;
            case "F" : // degres Fahrenheit 
                switch ($out) { 
                    case "C" : 
                        $convert = (($value - 32) * 5 / 9); 
                    break;
                    case "F" : 
                        $convert = $value; 
                    break;
                    case "K" : 
                        $convert = ((($value - 32) * 5 / 9) + 273.15); 
                    break;
                    case "RE" : 
                        $convert = (($value - 32) * 4 / 9); 
                    break;
                    case "RA" : 
                        $convert = ($value - 32 + 273.15 * 9 / 5); 
                    break;
                } 
            break;
            case "K" : // degres Kelvin 
                switch ($out) { 
                    case "C" : 
                        $convert = ($value - 273.15); 
                    break;
                    case "F" : 
                        $convert = (($value - 273.15) * 9 / 5 + 32);
                    break; 
                    case "K" : 
                        $convert = $value; 
                    break;
                    case "RE" : 
                        $convert = (($value - 273.15) * 4 / 5); 
                    break;
                    case "RA" : 
                        $convert = ($value * 1.8);
                    break;
                } 
            break;
            case "RE" : // degres Reaumur 
                switch ($out) { 
                    case "C" : 
                        $convert = ($value * 5 / 4); 
                    break;
                    case "F" : 
                        $convert = ($value * 9 / 4 + 32); 
                    break;
                    case "K" : 
                        $convert = ($value * 5 / 4 + 273.15); 
                    break;
                    case "RE" : 
                        $convert = $value; 
                    break;
                    case "RA" : 
                        $convert = (($value * 5 / 4 + 273.15) * 1.8);  
                    break;
                }
            break;
            case "RA" : // degres Rankine 
                switch ($out) { 
                    case "C" : 
                        $convert = ($value / 1.8 - 273.15); 
                    break;
                    case "F" : 
                        $convert = ($value - 273.15 * 9 / 5 + 32); 
                    break;
                    case "K" : 
                        $convert = ($value / 1.8); 
                    break;
                    case "RE" : 
                        $convert = (($value / 1.8 - 273.15) * 4 / 5); 
                    break;
                    case "RA" : 
                        $convert = $value; 
                    break;
                }
            break;            
        }

        return $round === null ? $convert : round($convert, $round);
    }


    /**
    * Convertion des vitesse
    * Unité de conversion disponible : MPH => mille par heure, KM/H => Kilomtre par heure
    *
    * @param float $value
    * @param string $in
    * @param string $out
    * @param integer $round Si null toutes les valeurs sont renvoyé
    * @return float
    */
    public static function speed(float $value, string $in, string $out, int $round = null)
    {
        $convert = "";        
        $out = strtoupper( $out );
        
        switch( strtoupper( $in ) ){
            case "MPH" :
                switch ( $out ){
                    case "MPH" :
                        $convert = $value;
                    break;                    
                    case "KM/H":
                        $convert = $value * 1.609344;
                    break;
                }
            break;                
            case "KM/H" :
                switch ( $out ){
                case "KM/H" :
                    $convert = $value;
                break;
                case "MPH":
                    $convert = $value * 0.62137119223733;
                break;
            }
            break;
        }
            
        return $round === null ? $convert : round($convert, $round);
    }

}
