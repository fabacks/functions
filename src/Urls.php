<?php
/**
*	@class \Fabacks\Functions\Urls
*	@description Classe utilitaire sur les URLs
*	@author Fabien COLAS
*	@site  dahoo.Fr
*	@gith https://github.com/Fabacks
*	@Copyright Licence CC-by-nc-sa 
*	@Update : 05/08/2019
*	@version 1.0.0
*/
namespace Fabacks\Functions;

Class Urls
{
    
	/**
	 * Définis si la chaine passée en parametre est une url ou non
	 *
	 * @param string $pURL
	 * @return boolean
	 */
	public static function isUrl($pURL)
	{
		$regex = '/^(http|https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,3}|www\.[^\s]+\.[^\s]{2,3})$/';
		return preg_match($regex, $pURL) ? true : false;
	}

	/**
	 * Retourne le parametre de l'url (clef => valeur)
	 *
	 * @param string $pUrl Url de réference
	 * @param string $pParameterName Nom du parametre à chercher 
	 * @return string
	 */
	public static function getValueFromUrl($pUrl, $pParameterName)
	{
		$lsValue = "";
		$url = str_replace("&amp;", "&", $pUrl);
		$parts = parse_url($url);
		
		if( isset($parts['query']) ):
			parse_str($parts['query'], $query);
			
			if( isset($query[$pParameterName]) ):			
				$lsValue = $query[$pParameterName];			
			endif;
		endif;

		return $lsValue;
	}

	/**
	 * Retourne l'url de la page actif
	 *
	 * @return string
	 */
	public static function getUrl() 
	{
		return $_SERVER['REQUEST_URI'];
	}

	/**
	 * Retourne la page courante dans l'url
	 *
	 * @param string $psUrl
	 * @return string
	 */
	public static function getPageUrl($psUrl = null) {		
		$page = (empty($psUrl) ? basename($_SERVER['REQUEST_URI']) : $psUrl);
		return explode("?", $page)[0];
	}
	
	/**
	 * Récupère le contenu brut d'une page web
	 *
	 * @param	$url					Url de la page à récupérer
	 * @param	$maximumRedirections	Nombre de redirection maximum (null par defaut, soit 1)
	 * @param	$currentRedirection		Nombre de redirections déja effectuées
	 *
	 * @return	string					Contenu de la page web
	 */
	public static function getUrlContents(string $url, $maximumRedirections = null, $currentRedirection = 0 ): string
	{
		$contents = false;
		
		if( extension_loaded( 'curl' ) && function_exists( 'curl_init' ) ) {		
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:19.0) Gecko/20100101 Firefox/19.0");
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
			$contents = curl_exec( $curl );
			curl_close( $curl );
		} else if( ini_get( 'allow_url_fopen' ) ) {
		
			$contents = @file_get_contents( $url, 'r' );			
		}
		
		if( isset($contents) && is_string($contents) ) {
			$reg = '/<[\s]*meta[\s]*http-equiv="?REFRESH"?' . '[\s]*content="?[0-9]*;[\s]*URL[\s]*=[\s]*([^>"]*)"?' . '[\s]*[\/]?[\s]*>/si';
			preg_match_all($reg, $contents, $match);
		
			if( isset($match) && is_array($match) && count($match) == 2 && count($match[1]) == 1 ) {
				
				if ( !isset($maximumRedirections ) || $currentRedirection < $maximumRedirections )
					return self::getUrlContents( $match[ 1 ][ 0 ], $maximumRedirections, ++$currentRedirection );
			}			
		}
	
		return $contents;
	}
}