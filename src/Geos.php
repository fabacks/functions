<?php

/**
*	@class \Fabacks\Functions\\Geos
*	@description Classe utilitaire sur la géolocalisation
*	@author Fabien COLAS
*	@site  dahoo.Fr
*	@gith https://github.com/Fabacks
*	@Copyright Licence CC-by-nc-sa 
*	@Update : 05/08/2019
*	@version 1.0.0
*/
namespace Fabacks\Functions;

Class Geos
{

    /**
    * Retourne la distance en (Miles|Kilometre|Nautiques) entre deux coordonnées géographique
    * Function d'origine https://www.geodatasource.com   
    *
    * @param float $lat1 latidude lieu 1
    * @param float $lon1 logitude lieu 1
    * @param float $lat2 latidude lieu 2 
    * @param float $lon2 logitude lieu 2
    * @param string $unit K pour kilometre, N pour nautique, M pour miles
    * @return float
    */
    public static function distance(float $lat1, float $lon1, float $lat2, float $lon2, string $unit = 'K') {
        if( ($lat1 == $lat2) && ($lon1 == $lon2) ) 
            return 0;        
        
        $theta = $lon1 - $lon2;
        $dist  = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist  = acos($dist);
        $dist  = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        switch( strtoupper($unit) ):
            case 'K':
                return ($miles * 1.609344);
            break;
            case 'N':
                return ($miles * 0.8684);
            break;
            case 'M':
                return $miles;
            break;
            default:
                return $miles;
            break;
        endswitch;
    }


}