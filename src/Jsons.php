<?php
/**
*	@class \Fabacks\Functions\Jsons
*	@description Classe utilitaire sur les json
*	@author Fabien COLAS
*	@site  dahoo.Fr
*	@gith https://github.com/Fabacks
*	@Copyright Licence CC-by-nc-sa 
*	@Update : 05/08/2019
*	@version 1.0.0
*/
namespace Fabacks\Functions;

Class Jsons
{
    
    
    /**
     * Test si le json est correcte
     *
     * @param string $string
     * @return boolean
     */
    public static function isValid($string) {
        // decode the JSON data
        // set second parameter boolean TRUE for associative array output.
        $result = json_decode($string);

        return json_last_error() === JSON_ERROR_NONE ? true : false;
    }


    /**
     * Test la validiter du Json et retourne l'erreur éventuel
     *
     * @param string $string
     * @return string
     */
	public static function validate($string)
    {
        // decode the JSON data
        $result = json_decode($string);

        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = ''; // JSON is valid // No error has occurred
            break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
            break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
            break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
            break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
            break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
            break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
            break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
            break;
            default:
                $error = 'Unknown JSON error occured.';
            break;
        }

        // everything is OK
        return $error == '' ? true : $error;
    }

}