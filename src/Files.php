<?php
/**
*	@class \Fabacks\Functions\\Files
*	@description Classe utilitaire sur les fichiers
*	@author Fabien COLAS
*	@site  dahoo.Fr
*	@gith https://github.com/Fabacks
*	@Copyright Licence CC-by-nc-sa 
*	@Update : 10/11/2020
*	@version 1.1.0
*/
Namespace Fabacks\Functions;

Class Files
{	
	/**
	 * Retourne l'extention du fichier grace à son chemin
	 *
	 * @param string $path
	 * @return string
	 */
	public static function pathToExtend($path)
	{
		return pathinfo($path, PATHINFO_EXTENSION);
	}	

	/**
	 * Retourne le nom du fichier grace à son chemin
	 *
	 * @param string $path
	 * @return string
	 */
	public static function pathToName($path)
	{
		return pathinfo($path, PATHINFO_FILENAME);
	}	

	/**
	 * Retourne le nom du fichier et son extension grace à son chemin
	 *
	 * @param string $path
	 * @return string
	 */
	public static function pathToBaseName($path)
	{
		return pathinfo($path, PATHINFO_BASENAME);
	}		
	
	/**
	 * Retourne les dimensions d'une image grace à son chemin
	 *
	 * @param string $path
	 * @return array
	 */
	public static function pathToImageSize($path)
	{
		$info = getimagesize($path);
		return array(
			'width'  => $info[0],
			'height' => $info[1],
		);
	}

	/**
	 * Dossier temporaire du système
	 *
	 * @return string
	 */
	public static function temporaryPath()
	{
		return sys_get_temp_dir();
	}

	/**
	 * Récupération de l'extension d'un fichier
	 * Méthode manuelle grâce au nom du fichier
	 * Méthode système voir pathToExtend
	 *
	 * @param string $file
	 * @return string
	 */
	public static function getExt($file)
	{
		$ext = explode('.', $file);
		return strtolower(array_pop($ext));
	}
	
	/**
	 * Retourne la taille maximum que l'on peut uploader dans le fichier php.ini
	 *
	 * @param boolean $readable convertie la taille octet en format lisible
	 * @return string
	 */
	public static function max_upload_size($readable = false)
	{
		$limits = array();
		$limits[]= (int)str_replace('M','',ini_get('post_max_size')) *1048576;
		$limits[]= (int)str_replace('M','',ini_get('upload_max_filesize')) *1048576;
		
		$min = min($limits);
		return ($readable ? self::readable_size($min) : $min);
	}
	
	/**
	 * Permet de rendre lisible une taille en bytes
	 *
	 * @param int $bytes taille en bytes
	 * @return string 
	 */
	public static function readable_size($bytes)
	{
		if($bytes<1024){
			return round(($bytes / 1024), 2).' o';
		}elseif(1024<$bytes && $bytes<1048576){
			return round(($bytes / 1024), 2).' Ko';
		}elseif(1048576<$bytes && $bytes<1073741824){
			return round(($bytes / 1024)/1024, 2).' Mo';
		}elseif(1073741824<$bytes){
			return round(($bytes / 1024)/1024/1024, 2).' Go';
		}

		return '';
	}

	/**
	  * Convertie une taille en bytes dans une unité choisie
	  *
	  * @param int $bytes
	  * @param string $pUnit Unité possible : O, K, M, G
	  * @param boolean $pUnitDisplay affiche l'unité de conversion
	  * @return float|string
	  */
	public static function convert_size($bytes, $pUnit = 'O', $pUnitDisplay = false)
	{
		$size  = $bytes;
		$unity = "";
		$roundPrecision = 4;
		
		switch( strtoupper($pUnit) ):
			case 'O':
				$size = $bytes;
				$unity = "o";
			break;
			case 'K':
				$size = round(($bytes / 1024), $roundPrecision);
				$unity = "Ko";
			break;
			case 'M':
				$size = round(($bytes / 1024)/1024, $roundPrecision);
				$unity = "Mo";
			break;
			case 'G':
				$size = round(($bytes / 1024)/1024/1024, $roundPrecision);
				$unity = "Go";
			break;
		endswitch;

		return $size.($pUnitDisplay ? ' '.$unity : '');
	}
	

	/**
	  * Permet de créer un nom de fichier propre

	  * @param string $pLabel Titre à formater
	  * @param int $pMaxChar Maximum de charactère (défaut 50)
	  * @param string $pCharReplace Caractère de remplacement (défaut '-')

	  * @return string
	  */
	public static function createFileName($pLabel, $pMaxChar = 50, $pCharReplace = "-") 
	{
		$label = str_replace("’", "'", $pLabel);
		$label = Strings::normalizeChars($label);
		$label = preg_replace("/[^a-zA-Z0-9\'\(\)\\s]/", $pCharReplace, $label);
		$label = preg_replace("/\s{2,}/", ' ', $label);
		$label = Strings::troncate($label, $pMaxChar, '');
		return $label;
	}

	/**
	 * Permet de telecharger un fichier distant sur le serveur
	 *
	 * @param string $destination chemin de destination
	 * @param string $urlDownload Url du fichier a telecharger
	 * @return bool
	 */
	public static function getFileUrl($destination, $urlDownload) {
		//On creer le fichier
		$fp = fopen($destination, 'w');

		//On initialise curl
		$ch = curl_init($urlDownload);
		curl_setopt($ch, CURLOPT_FILE, $fp);
		
		//On telecharge
		$data = curl_exec($ch);
		
		//On ferme le flux
		curl_close($ch);
		fclose($fp);

		return (file_exists($destination) ? true : false);
	}	
	
	/**
	 * Télécharge vers le client un fichier
	 * Trouvé sur : https://gist.github.com/aaronmcadam/428072
	 *
	 * @param string $pPath Chemin vers le fichier
	 * @param string $pFileName Nom du fichier
	 * @param bool $pDispoInline Si true essais de le télécharger dans le broswer
	 * @return void
	 */
	public static function downloadFile($pPath, $pFileName, $pDispoInline = false)
	{
		if( !file_exists($pPath) ) 
			throw new \Exception("Désolé, ce fichier n'existe pas ou plus.");

		$filesize		= filesize($pPath);
		$fileExtension 	= strtolower(substr(strrchr($pFileName,"."),1));
		$ctype = self::getContentType( $fileExtension );
		$contentDispo =  $pDispoInline ? 'inline' : 'attachment';
		ob_end_clean();

		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false); // required for certain browsers 
		header("Content-Type: $ctype");
		header("Content-Disposition: ".$contentDispo."; filename=\"".$pFileName."\"");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".$filesize);
		
		readfile($pPath);
	} 

	/**
	 * Télécharge vers le client un stream en mémoire
	 * Trouvé sur : https://gist.github.com/aaronmcadam/428072
	 *
	 * @param string $pStream Mémoire du fichier
	 * @param string $pFileName Nom du fichier
	 * @return void
	 */
	public static function downloadStream($pStream, $pFileName)
	{
		$fileExtension = strtolower(substr(strrchr($pFileName,"."),1));
		$ctype = self::getContentType( $fileExtension );

		ob_end_clean();

		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false); // required for certain browsers 
		header("Content-Type: $ctype");
		header("Content-Disposition: attachment; filename=\"".$pFileName."\";" );
		header("Content-Transfer-Encoding: binary");
		
		echo $pStream;
	} 

	/**
	 * Copie récursive de fichier / dossier 
	 *
	 * @param string $src path a copier
	 * @param string $dst path de destination
	 * @return void
	 */
	static public function recurse_copy($src, $dst)
	{ 
        $dir = opendir($src); 
		if( !file_exists($dst) )
			mkdir($dst, 0755, true);
		
		while( false !== ( $file = readdir($dir)) ): 
            if ( $file == '.' || $file == '..' ) continue; 
            
            if ( is_dir($src.DIRECTORY_SEPARATOR.$file) ) :
				if(!file_exists($dst.DIRECTORY_SEPARATOR.$file) ): 
					mkdir($dst.DIRECTORY_SEPARATOR.$file, 0755, true);
				endif;

				self::recurse_copy($src.DIRECTORY_SEPARATOR.$file, $dst.DIRECTORY_SEPARATOR.$file);
            else :         
                copy($src.DIRECTORY_SEPARATOR.$file, $dst.DIRECTORY_SEPARATOR.$file); 
            endif;              
		endwhile;
		
        closedir($dir); 
    }		
	
    /**
	 * Suppression recursive d'un dossier
	 *
	 * @param string $pDir chemin à supprimer
	 * @return bool success
	 */
	static public function recurse_delete($pDir)
	{
		// Si le chemin pointe sur rien, on sort
		if( !file_exists($pDir) ) return false;
		
		// Supprime si c'est un fichier
		if( is_file($pDir) ) return unlink($pDir);
		
		// Si le dossier pas readable on sort
		if( !is_readable($pDir) ) return false;

		// On test si le dossier est lisible
		$dirs = scandir($pDir);
		if( $dirs === false ) return false;
		
		// On supprime tous les fichiers du dossier		
		foreach($dirs as $item ):
            if( $item == '.' || $item == '..' ) continue;
			
			$path = $pDir.DIRECTORY_SEPARATOR.$item;
			if( !self::recurse_delete($path ) ):
				return false;
			endif;
		endforeach;
		
		// On test si le dossier est bien vide avant de le supprimer. 
		$dirs = scandir($pDir);
		return ($dirs === false || count($dirs) != 2 ? false : rmdir($pDir));
	}

	/**
	  * Retourne la taille du dossier en bytes
	  *
	  * @param string $dir
	  * @return float
	  */
	public static function folder_size($dir)
	{
		if( !file_exists($dir) ) return 0; 
		if( is_file($dir) )	return filesize($dir);
	
		$total_size = 0;
		$dir_array = scandir($dir);
		if($dir_array === false ) return 0;

		foreach($dir_array as $key => $filename):
			if( $filename == ".." ||  $filename == ".") continue;
			
			if( is_dir($dir."/".$filename) ){
				$total_size += self::folder_size($dir."/".$filename);
			}else if( is_file($dir."/".$filename) ){
				$total_size += filesize($dir."/".$filename);
			}		   
		endforeach;

		return $total_size;
	}

	/**
	  * Retourne la taille du fichier dans une unité de conversion
	  *
	  * @param string $pathFile
	  * @param string $pUnit Unité possible : O, K, M, G
	  * @param boolean $pUnitDisplay affiche l'unité de conversion
	  * @return float
	  */
	public static function file_sizeReadble($pathFile, $pUnit = 'O', $pUnitDisplay = false)
	{
		if( !file_exists($pathFile) || !is_file($pathFile) ) 
			return 0; 
		
		$bytes = filesize($pathFile);
		return self::convert_size($bytes, $pUnit, $pUnitDisplay);
	}

 	/**
	  * Retourne la taille du dossier dans une unité de conversion
	  *
	  * @param string $dir
	  * @param string $pUnit Unité possible : O, K, M, G
	  * @param boolean $pUnitDisplay affiche l'unité de conversion
	  * @return float
	  */
	public static function folder_sizeReadble($dir, $pUnit = 'O', $pUnitDisplay = false)
	{
		$bytes = self::folder_size($dir);	
		return self::convert_size($bytes, $pUnit, $pUnitDisplay);
	}

	/**
	 * Permet de creer une miniature d'une image
	 *
	 * @param string $pathToImages path de l'image original 
	 * @param string $pathToThumbs path de + nom de la miniature a creer
	 * @param int $thumbWidth dimension de la miniature en px
	 * @return void
	 */
	public static function createThumbs($pathToImages, $pathToThumbs, $thumbWidth = 60) {
		// Load info from image
		$info = pathinfo($pathToImages);
		$fileName = $info['basename'];
		$ext = pathinfo($fileName, PATHINFO_EXTENSION);

		// switch create with extension
		switch($ext):
			case 'jpg' :
			case '.jpg' :
			case 'jpeg' :
			case '.jpeg' :
				$img = imagecreatefromjpeg($pathToImages);
			break;
			case 'png' :
			case '.png' :
				$img = imagecreatefrompng($pathToImages);
			break;
			case 'gif' :
			case '.gif' :
				$img = imagecreatefromgif($pathToImages);
			break;
			case 'bmp' :
			case '.bmp' :
				$img = imagecreatefrombmp($pathToImages);
			break;
			default:
				return;
		endswitch;
			
		// load image and get image size
		$width = imagesx( $img );
		$height = imagesy( $img );

		// calculate thumbnail size
		$new_width = $thumbWidth;
		$new_height = floor( $height * ( $thumbWidth / $width ) );

		// create a new temporary image
		$tmp_img = imagecreatetruecolor( $new_width, $new_height );

		// copy and resize old image into new image 
		imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

		// save thumbnail into a file
		imagejpeg($tmp_img, $pathToThumbs);
	}

	/**
	 * Decode une image en base 64
	 *
	 * @param string $string String de image en base64
	 * @param string $pPathFile Chemin du fichier
	 */
	public static function base64_to_jpg($string, $pPathFile) 
	{
        $fp = fopen($pPathFile, "wb"); 
        $data = explode(',', $string);
        fwrite($fp, base64_decode($data[1])); 
        fclose($fp); 
	}


	/**
	 * Retourne le type du content-type
	 *
	 * @param string $pExtention
	 * @return string
	 */
	public static function getContentType($pExtention)
	{
		$ctype = '';
		$extention = strtolower($pExtention);
		switch( $extention ):
			case "pdf": 
				$ctype="application/pdf";
			break;
			case "exe": 
				$ctype="application/octet-stream";
			break;
			case "zip":
				$ctype="application/zip"; 
			break;
			case "doc":
			case "docx":
				$ctype="application/msword"; 
			break;
			case "xls":
			case "xlsx":
				$ctype="application/vnd.ms-excel"; 
			break;
			case "ppt":
			case "pptx":
				$ctype="application/vnd.ms-powerpoint"; 
			break;
			case "gif":
				$ctype="image/gif";
			break;
			case "png":
				$ctype="image/png";
			break;
			case "jpeg":
			case "jpg":
				$ctype="image/jpg";
			break;
			case "mp3":
				$ctype="audio/mpeg";
			break;
			default: 
				$ctype="application/force-download";
		endswitch;

		return $ctype;
	}

	/**
	 * Fournis des informations suivant le type d'icone
     * Icone via https://fontawesome.com/
	 *
	 * @param string $extention
	 * @return array
	 */
	public static function getExtInfo($extention) {
		$asoc = array();

		switch($extention){
			case '7z':
			case 'rar':
			case 'gz':
			case 'zip':
				$asoc["type"] = 'Archive';
				$asoc["icon"] = 'far fa-file-archive';
			break;
			
			case 'php':
			case 'js':
			case 'aspx':
			case 'py':
			case 'c':
			case 'cpp':
			case 'css':
			case 'h':
			case 'hpp':
			case 'html':
			case 'htm':
			case 'cs':
			case 'asp':
			case 'jsp':
				$asoc["type"] = 'Code';
				$asoc["icon"] = 'far fa-file-code';
			break;
			
			case 'xls':
			case 'xlsx':
			case 'csv':
				$asoc["type"] = 'Excel';
				$asoc["icon"] = 'far fa-file-excel';
			break;
			
			case 'bmp':
			case 'jpg':
			case 'jpeg':
			case 'ico':
			case 'gif':
			case 'png':
			case 'svg':
				$asoc["type"] = 'Image';
				$asoc["icon"] = 'far fa-file-image';
			break;
			
			case 'pdf':
				$asoc["type"] = 'PDF';
				$asoc["icon"] = 'far fa-file-pdf';
			break;

			case 'ppt':
			case 'pptx':
				$asoc["type"] = 'PowerPoint';
				$asoc["icon"] = 'far fa-file-powerpoint';
			break;
			
			case 'txt':
			case 'htaccess':
			case 'md':
				$asoc["type"] = 'Texte';
				$asoc["icon"] = 'far fa-file-text';
			break;
			
			case 'doc':
			case 'docx':
			case 'word':
				$asoc["type"] = 'Word';
				$asoc["icon"] = 'far fa-file-word';
			break;
			
			case 'avi':
			case 'wmv':
			case 'mov':
			case 'divx':
			case 'xvid':
			case 'mkv':
			case 'flv':
			case 'mpeg':
			case 'h264':
			case 'rmvb':
			case 'mp4':
				$asoc["type"] = 'Video';
				$asoc["icon"] = 'far fa-file-movie';
			break;
			
			case 'wav':
			case 'ogg':
			case 'ogv':
			case 'ogx':
			case 'oga':
			case 'riff':
			case 'bwf':
			case 'wma':
			case 'flac':
			case 'aac':
			case 'mp3':
				$asoc["type"] = 'Musique';
				$asoc["icon"] = 'far fa-file-audio';
			break;

			default:
				$asoc["type"] = 'Inconnu';
				$asoc["icon"] = 'far fa-file';
			break;
		}

		return $asoc;
	}
        
    
	/**
	 * Returne la font associé à l'extension (font-awesome)
     * Icone via https://fontawesome.com/
	 *
	 * @param string $ext
	 * @return string
	 */
	public static function getExtIcon($ext) {
		$info = self::getExtInfo($ext);

		return $info["icon"];
	}


	/**
	 * Returne le type de fichier associé à l'extension
	 *
	 * @param string $ext
	 * @return string
	 */
	public static function getExtType($ext) {
		$info = self::getExtInfo($ext);

		return $info["type"];
	}


	/**
	 * Permet de determiner le format d'une image
	 * Retourne "portrait" pour un portrait
	 * Retourne "landscape" pour un payage
	 *
	 * @param string $pPath de l'image
	 * @return string
	 */
	public static function imageFormat($pPath) {
		if( !file_exists($pPath) )
			return "";
			
		$dim = getimagesize($pPath);
		$width  = $dim[0];
		$height = $dim[1];

		return ($width / $height >= 1 ? 'landscape' : 'portrait');
	}
	
}