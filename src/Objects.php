<?php

/**
*	@class \Fabacks\Functions\Objects
*	@description Classe utilitaire sur les objets / array
*	@author Fabien COLAS
*	@site  dahoo.Fr
*	@gith https://github.com/Fabacks
*	@Copyright Licence CC-by-nc-sa 
*	@Update : 05/08/2019
*	@version 1.0.0
*/

namespace Fabacks\Functions;

Class Objects
{
    
    /**
	 * Permet de créer une liste option et option groupe a partir d'un json
	 [
    	{
        	"label": "Affaire générales",
        	"slug": "affaire_generale",
        	"child": [
            	{
                	"label": "affaires juridiques",
                	"slug": "affaire_juridique"
				}
			]
		}
	 ]
	 * @param array $pJson
	 * @param string $pnValueSelected
	 * @return string
	 */
	public static function getJsonToCbo($pJson, $pnValueSelected = "")
	{
		$stream = "";

		foreach($pJson as $item):
			if( empty($item["child"]) || count($item["child"]) == 0 ) :
				$select = ($item["value"] == $pnValueSelected ? 'selected="selected"' : '');
				$stream .= '<option value="'.$item["value"].'" '.$select.' >'.$item["label"].'</option>';
			else :    
				$stream .= '<optgroup label="'.$item["label"].'">';
				$stream .= self::getJsonToCbo($item["child"], $pnValueSelected);
				$stream .= '</optgroup>';
			endif;
		endforeach;

		return $stream;
	} 

	/**
	 * Retourne un array pour remplir un combobox avec séléction de l'item et gestion des non actif
	 *
	 * @param array $plArray est array d'object avec toutes les valeurs
	 * @param string $pnValSelected  id de l'élément qui doit être selectionné
	 * @param boolean $itemEmpty Determine si l'on doit inserer un élément de séléction
	 * @param array $in Array permettant de définir le nom des attributs d'entrée => array("id", "label", "active")
	 * @param array $out Array permettant de définir le nom des attributs de sortie => array("value", "label", "selected")

	 * @return array Champs de retour par défaut (value => string, label => string, active => boolean)
	 */
	public static function  getCbo($plArray, $pnValSelected = "", $itemEmpty = false, $in = array(), $out = array() ) 
	{
		//Array de retour
		$arrayValue = array();

		//Membre d'entrée
		$in_val = ($in[0] ?? "id");
		$in_lab = ($in[1] ?? "label");
		$in_act = ($in[2] ?? "active");

		//Membre de sortie
		$out_val = ($out[0] ?? "value");
		$out_lab = ($out[1] ?? "label");
		$out_sel = ($out[2] ?? "selected");
		
		//Si pas d'élément on retour rien
		if(empty($plArray) || (is_array($plArray) && count($plArray) == 0) ) :
			return array("$out_val" => 0, "$out_lab" => "---", "$out_sel" => true);
		endif;

		//Si on insert un élément vide de sélection
		if( $itemEmpty ) $arrayValue[] = array("$out_val" => 0, "$out_lab" => "---", "$out_sel" => false);

		//Un item est sélectionné
		$isSelected = false;

		//Boucle sur le tableau d'entrée
		foreach($plArray as $child):
			$item = (array)$child;

			//Permet de faire apparaitre dans une liste un element supprimer si sélectionné
			if( isset($item["$in_act"]) && (bool)$item["$in_act"] == false && $item["$in_val"] != $pnValSelected ) continue;
			
			//Permet de déterminé si un item est sélectionné
			$itemSelected = false;
			if( $item[$in_val] == $pnValSelected ):
				$itemSelected = true;
				$isSelected = true;
			endif;
					

			//Item de sortie
			$arrayValue[] = array(
				"$out_val"	=> $item["$in_val"],
				"$out_lab"	=> $item["$in_lab"],
				"$out_sel"  => $itemSelected
			);
		endforeach;

		//Si rien de sélectionner, on sélectionne le premier
		if( !$isSelected ):
			$arrayValue[0][$out_sel] = true;
		endif;

		return $arrayValue;           
	}

	/**
	 * Permet de retourner toutes les property d'un objet en array avec le nom => valeur
	 *
	 * @param object $poObject
	 * @param array $plIgnoreProperty
	 * @return array
	 */
	public static function objetToArray($poObject, $plIgnoreProperty = array() ) {
		$loArray = array();
		
		//Liste des property a ignorer
		$llIgnoreProperty = array("lastError", "lastQuery");
		$llIgnoreProperty = array_merge($llIgnoreProperty, $plIgnoreProperty);
		$llIgnoreProperty = array_unique($llIgnoreProperty);
		
		//Si vide, on revoie tableau vide
		if(!$poObject) return $loArray;

		foreach ($poObject as $name => $value) :
			if(in_array( $name, $llIgnoreProperty)) continue;
			
			$loArray[$name] = $value;
		endforeach;

		return $loArray;
    }
}