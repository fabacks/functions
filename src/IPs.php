<?php

/**
*	@class \Fabacks\Functions\Ips
*	@description Classe utilitaire sur les IPs
*	@author Fabien COLAS
*	@site  dahoo.Fr
*	@gith https://github.com/Fabacks
*	@Copyright Licence CC-by-nc-sa 
*	@Update : 05/08/2019
*	@version 1.0.0
*/
namespace Fabacks\Functions;

Class IPs
{

    /**
     * Retourne l'adresse IP du client
     *
	 * @view https://www.virendrachandak.com/techtalk/getting-real-client-ip-address-in-php-2/
     * @return string
     */
    public static function ipClient() {
		$ipaddress = '';
		if( !empty($_SERVER['HTTP_CLIENT_IP']) )
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if( !empty($_SERVER['HTTP_X_FORWARDED_FOR']) )
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if( !empty($_SERVER['HTTP_X_FORWARDED']) )
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if( !empty($_SERVER['HTTP_FORWARDED_FOR']) )
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if( !empty($_SERVER['HTTP_FORWARDED']) )
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if( !empty($_SERVER['REMOTE_ADDR']) )
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';

		return $ipaddress;
    }

    /**
     * Test si la string est une IP valide V4 ou V6
     *
     * @param string $ip
     * @return boolean
     */
    public static function isIp($ip) 
	{
        return filter_var($ip, FILTER_VALIDATE_IP) ? true : false;
    }

	/**
	 * Determine si l'utilisateur courant ou la chaine passer en paramètre et en localhost
	 *
	 * @param string $psRemoteAddress
	 * @return boolean
	 */
	public static function isLocalhost($psRemoteAddress = null) 
	{
		$address = empty($psRemoteAddress) ? $_SERVER['REMOTE_ADDR'] : $psRemoteAddress;
		return in_array($address, array('127.0.0.1', '::1'));
	}

	/**
	 * Determine si l'utilisateur courant ou la chaine passer en paramètre n'est pas en localhost
	 *
	 * @param string $psRemoteAddress
	 * @return boolean
	 */
	public static function isNotLocalhost($psRemoteAddress = null) {
		return !self::isLocalhost($psRemoteAddress);
	}

    /**
	 * Retourne les informations relatives à une adresse IP V4
	 *
	 * GeoPlugin WebService
	 * @see http://www.geoplugin.net
	 *
	 * @param string $ipv4	Adresse IP V4 à rechercher, NULL pour utiliser son adresse IP
	 * @return mixed		Retourne un tableau contenant les données ou NULL en cas d'échec
	 */
	public static function ipToCoords_geoPlugin(string $ipv4 = null ) 
	{
		if( is_null( $ipv4 ) )
			$ipv4 = self::ipClient();
	
		$url = "http://www.geoplugin.net/php.gp?ip=".$ipv4;
	
		$serialized = Urls::getUrlContents( $url );
		if( is_null($serialized) || $serialized == '' )
			return null;
		
		$result = json_decode( str_replace( 'geoplugin_', '', json_encode( unserialize( $serialized ) ) ), true );
		if( is_array($result) && isset( $result['status'] ) && $result['status'] == 200 )
			return $result;
        
		return null;
	}
	
	
	/**
	 * Retourne les informations relatives à une adresse IP V4 ou V6
	 *
	 * Geoip DB WebService
	 * @see https://geoip-db.com
	 *
	 * @param string $ip		Adresse IP V4 ou V6 à rechercher, NULL pour utiliser son adresse IP
	 * @return mixed		Retourne un tableau contenant les données ou NULL en cas d'échec
	 */
	public static function ipToCoords_geoipDB(string $ip = null ) 
	{
        if( is_null( $ip ) )
            $ip = self::ipClient();

		$url = "https://geoip-db.com/json/".$ip;
		$json = Urls::getUrlContents( $url );
		if( is_null( $json ) || $json == '' )
			return null;

		return json_decode($json, true);
	}
	
	
	/**
	 * Retourne les informations relatives à une adresse IP V4
	 *
	 * IPWHOIS WebService
	 * @see https://ipwhois.io
	 *
	 * @param string $ipv4	Adresse IP V4 à rechercher, NULL pour utiliser son adresse IP
	 * @return mixed		Retourne un tableau contenant les données ou NULL en cas d'échec
	 */
	public static function ipToCoords_IPWHOIS(string $ipv4 = null ) 
	{
        if( is_null( $ipv4 ) )
            $ipv4 = self::ipClient();
	
		$url = "http://free.ipwhois.io/json/".$ipv4;
	
		$json = Urls::getUrlContents($url);
		if( is_null( $json ) || $json == '' )
			return null;

		$result = json_decode($json, true);
		if( is_array($result) && isset( $result['success'] ) && $result['success'] )
			return $result;
			
		return null;
	}
}