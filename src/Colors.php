<?php
/**
*	@class \Fabacks\Functions\Colors
*	@description Classe utilitaire sur les couleurs
*	@author Fabien COLAS
*	@site  dahoo.Fr
*	@gith https://github.com/Fabacks
*	@Copyright Licence CC-by-nc-sa 
*	@Update : 05/08/2019
*	@version 1.0.0
*/
namespace Fabacks\Functions;

Class Colors
{
	/**
	 * Définis si la chaine passée en paramètre est une couleur hexadécimale
	 * Define if the string is a colour
	 *
	 * @param string $hexcode
	 * @return boolean
	 */
	public static function isColorHexa($hexcode)
	{
		return preg_match('/^#(?:(?:[a-f\d]{3}){1,2})$/i', $hexcode) ? true : false;
	}


    /**
	 * Convertie un code hexa en code RGB
	 * Convert code hexa to RGB
	 *
	 * @param string $hexcode
	 * @return array
	 */
	public static function hexaToRGB($hexcode)
	{
		if( !self::isColorHexa($hexcode) ) 
			return array();

		$hexcode = substr($hexcode, 1);	    
		$hex1 = hexdec($hexcode[0] . $hexcode[1]);
		$hex2 = hexdec($hexcode[2] . $hexcode[3]);
		$hex3 = hexdec($hexcode[4] . $hexcode[5]);

		return array($hex1,	$hex2, $hex3);    
	}

	
	/**
	 * HSL algorithm pour définir si une couleur est claire ou foncé
	 * HSL algorithm for define if the light is clair or dark
	 * Couleur blanche 1 | Couleurs noir 0
	 *
	 * @param string $hexcode
	 * @return float between 0-1 Intensité de couleur
	 */
	public static function getLight($hexcode, $treshold = 510.0)
	{
		if( !self::isColorHexa($hexcode) ) return 0.5;
		
		$rgb = self::hexaToRGB($hexcode);
		$max = max($rgb[0], $rgb[1], $rgb[2]);
		$min = min($rgb[0], $rgb[1], $rgb[2]);
		
		return ($max + $min) / $treshold; 
    }
}