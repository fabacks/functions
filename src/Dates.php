<?php
/**
*	@class \Fabacks\Functions\Dates
*	@description Classe utilitaire sur les dates
*	@author Fabien COLAS
*	@site http://dahoo.Fr
*	@gith https://github.com/Fabacks
*	@Copyright Licence CC-by-nc-sa 
*	@Update : 05/08/2019
*	@version 1.0.0
*/
namespace Fabacks\Functions;

class Dates
{	
	/**
	 * Détermine si la date passer est valide
	 *
	 * @param string $date
	 * @param string $format Format de la date (DMY => jour, mois, année | MDY => mois, année, jour | YMD => année, moi, jour)
	 * @return boolean
	 */
	public static function isDate($date, $format = "DMY")
	{
		if( trim($date) == '')
			return false;

		$replace = array('-',' ','\\');
		$date = str_replace($replace, '/', trim($date));
		
		switch( mb_strtoupper($format) ):
			case 'DMY' :
				list($d, $m, $y) = explode('/', $date);
			break;
			case 'MDY' :
				list($m, $d, $y) = explode('/', $date);
			break;
			case 'YMD' :
				list($y, $m, $d) = explode('/', $date);
			break;
			default :
				return false;
			break;
		endswitch;

		if( !is_int($m) || !is_int($d) || !is_int($y) )
			return false;
		
		return checkdate($m, $d, $y);
	}

	/**
	 * Retourne une date formater
	 *
	 * @param integer $timestamp
	 * @return string date
	 */
	public static function timestampToDate($timestamp)
	{
		$d = date("d", $timestamp);
		$m = self::getFullMonthName( date("n", $timestamp) );
		$y = date("Y", $timestamp);
		$h = date("H:i:s", $timestamp);

		return "$d $m $y à $h";
	}

	/**
	* Retourne un timestamp en fonction d'une date textuelle au format Français
	*
	* @param string $date Date au format dd/mm/Y (h:i:s optionnel)
	* @param bool $takeTime Si true, permet de définir les heures, minutes, seconde
	* @return int|false variable sécurisée
	*/
	public static function toTime($date, $takeTime = false)
	{		
		if( trim($date) == '') 
			return false;

		$date = str_replace(array('-',' ','\\'),'/',trim($date));

		if(substr_count($date, '/') <2 ) 
			return false;

		$infos = explode('/',$date);
		$d = $infos[0];
		$m = $infos[1];
		$y = $infos[2];
		$h = $i = $s = 0;

		if( $y < 1902 || $y > 2038 ):
			throw new \Exception("Merci de sélectionner une date comprise entre 1902 et 2038.");
		endif;

		if( $takeTime ):
			$timeInfos = explode(':',$infos[3]);
			if(isset($timeInfos[0])) $h = $timeInfos[0];
			if(isset($timeInfos[1])) $i = $timeInfos[1];
			if(isset($timeInfos[2])) $s = $timeInfos[2];
		endif;		

		return strtotime($y.'-'.$m.'-'.$d.' '.$h.':'.$i.':'.$s);
	}




	/**
	 * Retourne si une année est bissextile
	 *
	 * @param integer $year	Année a tester
	 * @return boolean true si l'année est bissextile, sinon false
	 */
	public static function isLeapYear(int $year): bool
	{
		return $year % 4 == 0 && ($year % 100 != 0 || $year % 400 == 0);
	}

	/**
	 * Retourne la liste des mois abrégés Français
	 * Janvier commence par 1 et Décembre fini par 7
	 * Format en PHP : n
	 * 
	 * @return array  clef numéro du mois | value mois 
	 */
	public static function getMonthNamesShort()
	{
		return array(1 => "Jan", 2 => "Fév", 3 => "Mar", 4 => "Avr", 5 => "Mai", 6 => "Juin", 7 => "Juil", 8 => "Aoû", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Déc");
	}

	/**
	 * Retourne le mois abrégés Français
	 *
	 * @param integer $month numéro du mois
	 * @return string
	 */
	public static function getMonthNameShort(int $month) 
	{
		return self::getMonthNamesShort()[$month] ?? '';
	}

	/**
	 * Retourne la liste des mois francais
	 * Janvier commence par 1 et Décembre fini par 7
	 * Format en PHP : n
	 * 
	 * @return array clef numéro du mois | value mois
	 */
	public static function getFullMonthNames()
	{
		return array(1 => "Janvier", 2 => "Février", 3 => "Mars", 4 => "Avril", 5 => "Mai", 6 => "Juin", 7 => "Juillet", 8 => "Août", 9 => "Septembre", 10 => "Octobre", 11 => "Novembre", 12 => "Décembre");
	}

	/**
	 * Retourne le mois francais
	 *
	 * @param int $month numéro du mois
	 * @return string
	 */
	public static function getFullMonthName(int $month) 
	{
		return self::getFullMonthNames()[$month] ?? '';
	}

	/**
	 * Retourne la liste des premières lettres des jours de la semaine francais
	 * Lundi commence par 1 et dimanche fini par 7
	 * Format en PHP : N
	 * 
	 * @return array clef jour de la semaine abrégé en anglais | value premiere lettre de la semaine
	 */
	public static function getNumberDayNamesFirstLetter()
	{
		return array('1'=>'L', '2'=>'M', '3'=>'M', '4'=>'J', '5'=>'V', '6'=>'S', '7'=>'D');
	}

	/**
	 * Retourne la premiere lettre du jour francais
	 * Format en PHP : D
	 * 
	 * @param string $day jour abrégé en anglais
	 * @return string
	 */
	public static function getNumberDayNameFirstLetter(string $day)
	{
		return self::getNumberDayNamesFirstLetter()[$day] ?? '';
	}

	/**
	 * Retourne la liste des premières lettres des jours de la semaine francais
	 * Format en PHP : D
	 * 
	 * @return array clef jour de la semaine abrégé en anglais | value premiere lettre de la semaine
	 */
	public static function getDayNames()
	{
		return array('Mon'=>'L','Tue'=>'M','Wed'=>'M','Thu'=>'J','Fri'=>'V','Sat'=>'S','Sun'=>'D');
	}

	/**
	 * Retourne la premiere lettre du jour francais
	 *
	 * @param string $day jour abrégé en anglais
	 * @return string
	 */
	public static function getDayName(string $day)
	{
		return (self::getDayNames()[$day] ?? "");
	}

	/**
	 * Retourne la liste des jours de la semaine francais
	 * Format en PHP : D
	 * 
	 * @return array value string abrégé du jour en anglais | value le jour
	 */
	public static function getFullDayNames()
	{
		return array('Mon'=>'Lundi','Tue'=>'Mardi','Wed'=>'Mercredi','Thu'=>'Jeudi','Fri'=>'Vendredi','Sat'=>'Samedi','Sun'=>'Dimanche');
	}

	/**
	 * Retourne le jour de la semaine francais
	 * Format en PHP : D
	 * 
	 * @param string $day le jour abrégé en anglais
	 * @return string
	 */
	public static function getFullDayName(string $day)
	{
		return self::getFullDayNames()[$day] ?? '';
	}

	/**
	 * Retourne la liste des jours de la semaine francais
	 * Lundi commence par 1 et dimanche fini par 7
	 * Format en PHP : N
	 * 
	 * @return array key numéro jours | value le jour
	 */
	public static function getNumberDayNames()
	{
		return array('1'=>'Lundi', '2'=>'Mardi', '3'=>'Mercredi', '4'=>'Jeudi', '5'=>'Vendredi', '6'=>'Samedi', '7'=>'Dimanche');
	}

	/**
	 * Retourne le jour de la semaine francais au format entier exemple "Lundi"
	 * Lundi commence par 1 et dimanche fini par 7
	 * Format en PHP : N
	 *
	 * @param int $day numéro du jour
	 * @return string
	 */
	public static function getNumberDayName(int $day)
	{
		return self::getNumberDayNames()[$day] ?? '';
	}


















	/**
     * Retourne le nombre de semaines sur l'année en paramètre
     * 
     * @param mixed $year 
     * @return int 
     */
    public static function number_of_weeks_in_year($year)
    {
        $date = new \DateTime();
        $date->setISODate($year, 53);
        return $date->format("W") === "53" ? 53 : 52;
    }

    /**
     * Retourne le numéro du mois en fonction du numéro de semaine et l'année
     * 
     * @param mixed $week 
     * @param mixed $year 
     * @return int 
     */
    public static function get_month_from_week_number($week, $year) 
    {
        $week_start = new \DateTime();
        $week_start->setISODate($year, $week);
        return (int)$week_start->format("m");
    }

    /**
     * Retourne le jours de la semaine en fonction du numéro de semaine et de l'année 
     * 
     * @param mixed $week_number 
     * @param mixed $year 
     * @return array 
     */
    public static  function display_week_days($week_number, $year) 
    {
        $days_of_week = self::getNumberDayNames();
        $number_of_days = 7;

        $week = array();

        // Calculer la date du premier jour de la semaine
        $first_day = new \DateTime();
        $first_day->settime(0,0);
        $first_day->setISODate($year, $week_number);

        for ($i = 0; $i < $number_of_days; $i++) {
            $day_number = ($week_number - 1) * $number_of_days + $i + 1;
            $day_of_week = $days_of_week[$i];

            $day = clone $first_day;
            $day->modify("+$i day");

            $week[] = array(
                'day_number' => $day_number,
                'dayWeek'    => $day_of_week,
                'label'      => $day->format("d/m/Y"),
                'timestamp'  => $day->getTimestamp(),
            );
        }

        return $week;
    }


	/**
	 * Convertie des secondes en Heures, minutes, secondes
	 * Renvoie un tableau array(hours, minutes, secondes, invert)
	 * 
	 * @param int $pSeconds 
	 * @return (float|int|bool)[] 
	 */
	public static function convert_seconds_to_timeArray($pSeconds)
	{
		$seconds = abs($pSeconds);

		$hours = floor($seconds / 3600);
		$minutes = floor(($seconds % 3600) / 60);
		$seconds = $seconds % 60;

        return array(
            "hours"     => $hours, 
            "minutes"   => $minutes, 
			"secondes"	=> $seconds,
            'invert'    => $pSeconds < 0 ? true : false,
        );
	}

	/**
	 * Convertie le retour de la fonction convertSecondsToTime en time de string
	 * 
	 * @param int $pTime 
	 * @param bool $pWithSec Affichage des secondes 
	 * @return string 
	 */
	public static function convert_seconds_to_timeString($pTime, $pWithSec = true) 
	{
		$time = self::convert_seconds_to_timeArray($pTime);

		// echo \PHP_EOL;
		// echo "Input : ".$pTime.\PHP_EOL;
		// echo "Heures : ".$time['hours']." || Min : ".$time["minutes"].' || Sec : '.$time["secondes"].' || Négatif : '.($time['invert'] ? 'Oui' : 'Non').\PHP_EOL;

		$hours = ($time['hours'] < 10 ? '0' : '').$time['hours'];
		$min   = ($time["minutes"] < 10 ? '0' : '').$time["minutes"];
		$sec   = ($time["secondes"] < 10 ? '0' : '').$time["secondes"];

        return ($time['invert'] == true ? '-' : '').$hours.':'.$min.($pWithSec ? ':'.$sec : '');
	}

	/**
	 * Renvoie un tableau avec Heures, minutes, secondes, négative en fonction d'une heure au format "00:00" ou "00:00:00:"
	 * 
	 * @param string $time 
	 * @return (int|string|bool)[] 
	 */
	public static function convert_time_to_info($time) 
	{
		$isNegative = $time[0] == '-' ? true : false;
		$expl = explode(':', $time); 

		$h = abs($expl[0]);
		$m = $expl[1];
		$s = $expl[2] ?? 0;

		// echo \PHP_EOL;
		// echo 'Time input : '.$time.\PHP_EOL;
		// echo 'Heure : '.$h.' ||Min : '.$m.' || Sec : '.$s.' || Negatif : '.($isNegative ? 'Oui' : 'Non');
		// echo \PHP_EOL;

		return array('hours' => (int)$h, "minutes" => (int)$m, "seconds" => (int)$s, 'invert' => $isNegative);
	}

	/**
	 * Convertie une heure en secondes
	 * 
	 * @param string $pTime 
	 * @return int 
	 */
	public static function convert_time_to_secondes($pTime)
	{
		$start = self::convert_time_to_info($pTime);
		$secondes = ((int)$start['hours'] * 3600) + ((int)$start['minutes'] * 60) + (int)$start['seconds'];
		return $start['invert'] ? $secondes * -1 : $secondes;
	}


    /**
     * Calcule la différence entre deux heures au format 00:00
     * 
     * @param string $pStart_time 
     * @param string $pEnd_time 
     * @return string
     */
    public static function calculate_time_difference($pStart_time, $pEnd_time) 
    {
		$start = self::convert_time_to_secondes($pStart_time);
		$end = self::convert_time_to_secondes($pEnd_time);

		$diff = $start - $end;
		$resp =  self::convert_seconds_to_timeString($diff);

		// echo \PHP_EOL;
		// echo "Start : ".$start." || End : ".$end.\PHP_EOL;
		// echo "Diff : ".$diff.\PHP_EOL;
		// echo "Trans : ".$resp.\PHP_EOL;

		return $resp;
    }


    /**
     * Additionne deux heures entre elle
     * @param mixed $time1 
     * @param mixed $time2 
     * @return string 
     */
    public static function calculate_time_add($time1, $time2)
    {
        $dtStart = self::convert_time_to_secondes($time1);
        $dtEnd = self::convert_time_to_secondes($time2);

        $resp = $dtStart + $dtEnd;
        return self::convert_seconds_to_timeString($resp);
    }

	/**
	 * Soustrait deux heures entre elles
	 * 
	 * @param string $time1 
	 * @param string $time2 
	 * @return string 
	 */
    public static function calculate_time_subtract($time1, $time2) 
    {
        $dtStart = self::convert_time_to_secondes($time1);
        $dtEnd = self::convert_time_to_secondes($time2);

        $resp = $dtStart - $dtEnd;
        return self::convert_seconds_to_timeString($resp);
    }


	/**
	 * Permet de calculer une suite d'heure entre elle, soit en positif soit en négatif
	 * Exemple : 03:00 - 01:00 + 00:30 = 02:30
	 * 
	 * @param array $times Un array de toutes les heures
	 * @return string 
	 */
	public static function calculate_sum_times($times)
	{
		$timeCalc = 0;

		foreach($times as $time):
			$timeCalc += self::convert_time_to_secondes($time);
		endforeach;

		return self::convert_seconds_to_timeString($timeCalc);
	}

    /**
     * Détermine si l'heure de début et plus petite que l'heure de fin
	 * 
     * @param string $time1 au format H:mm ou H:mm:ss
     * @param string $time2 au format H:mm ou H:mm:ss
     * @return bool True si l'heure de début < heure de fin
     */
    public static function compare_hours($time1, $time2) 
    {
        return self::convert_time_to_secondes($time1) < self::convert_time_to_secondes($time2) ? true : false;
    }

}