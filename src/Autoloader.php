<?php 

class Functions_Autoloader 
{
    private $baseDir;

    /**
     * Autoloader constructor.
     *
     * @param string $baseDir Functions library base directory (default: dirname(__FILE__).'/..')
     */
    public function __construct($baseDir = null) 
    {
        if ($baseDir === null) {
            $baseDir = __DIR__;
        }

        // realpath doesn't always work, for example, with stream URIs
        $realDir = realpath($baseDir);
        if (is_dir($realDir)) {
            $this->baseDir = $realDir;
        } else {
            $this->baseDir = $baseDir;
        }
    }

    /**
     * Register a new instance as an SPL autoloader.
     *
     * @param string $baseDir base directory (default: dirname(__FILE__).'/..')
     *
     * @return object Registered Autoloader instance
     */
    public static function register($baseDir = null) 
    {
        $loader = new self($baseDir);
        spl_autoload_register(array($loader, 'autoload'));

        return $loader;
    }

    /**
     * Autoload classes.
     *
     * @param string $class
     */
    public function autoload($class) 
    {
        if ($class[0] === '\\') {
            $class = substr($class, 1);
        }

        if (strpos($class, 'Fabacks\Functions') !== 0) {
            return;
        }

        $sep = explode('\\',  $class);
        $file = $this->baseDir.DIRECTORY_SEPARATOR.$sep[2].'.php';
        if (is_file($file)) :
            require $file;
        endif;
    }
}