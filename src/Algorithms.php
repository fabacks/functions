<?php
/**
*	@class Functions\Colors
*	@description Classe utilitaire sur les couleurs
*	@author Fabien COLAS
*	@site  dahoo.Fr
*	@gith https://github.com/Fabacks
*	@Copyright Licence CC-by-nc-sa 
*	@Update : 05/08/2019
*	@version 1.0.0
*/
namespace Fabacks\Functions;

Class Algorithms
{

    /**
     * Algorithme de Luhn
     * 
     * @param mixed $pString 
     * @return mixed 
     */
    public static function Luhn($pString)
    {
        $total = 0;
        for($i = 0; $i < 14; $i++) {
          $temp = substr($pString, $i, 1);
          if ($i % 2 == 0) {
            $temp *= 2;
            if ($temp > 9) {
              $temp -= 9;
            }
          }
          
          $total += $temp;
        }
        return $total;
    }
}