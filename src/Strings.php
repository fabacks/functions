<?php
/**
*	@class \Fabacks\Functions\Strings
*	@description Classe utilitaire sur les strings / chaines de caractères
*	@author Fabien COLAS
*	@site  dahoo.Fr
*	@gith https://github.com/Fabacks
*	@Copyright Licence CC-by-nc-sa 
*	@Update : 26/09/2022
*	@version 1.1.0
*/
namespace Fabacks\Functions;

Class Strings
{
    
    /**
	 * Permet d'assembler les éléments d'un tableau en une chaîne
	 *
	 * @param string $glue element séparateur du rassemblement
	 * @param array $array array de string
	 * @param int $offsetStart commencer le tableau à un element particulier, si null alors on commence au debut
	 * @param int $offsetEnd fini le tableau à un element particulier, si null on fini avec le dernier element
	 * @return string
	 */
	public static function implode(string $glue, array $array, int $offsetStart = null, int $offsetEnd = null)
	{
		if( $offsetStart == null && $offsetEnd == null )
			return implode($glue, $array);
		
		$count = count($array) - 1;		
		
		if( $offsetStart == null )
			$offsetStart = 0;

		if( $offsetStart > $count)
			return "";
		
		if( $offsetEnd == null || $offsetEnd > $count )
			$offsetEnd = $count;

		return implode($glue, array_slice($array, $offsetStart, $offsetEnd) );
	}


	/**
	 * Permet de mettre en majuscule un text francais avec des accents
	 * Légèrement plus lent que strtoupper natif mais fonctionne avec tous
	 *
	 * @param string $string
	 * @return string
	 */
	public static function strtoupper($string, $encoding = "UTF-8") {
		return $string == null ? '' : mb_strtoupper($string, $encoding);
	}


	/**
	 * Permet de mettre en minuscule un text francais avec des accents
	 * Légèrement plus lent que strtolower natif mais fonctionne avec tous
	 *
	 * @param string $string
	 * @return string
	 */
	public static function strtolower($string, $encoding = "UTF-8") {
		return $string == null ? '' : mb_strtolower($string, $encoding);
	}


	/** 
     * Permet de mettre toutes les premières lettre des mots d'une phrase en majuscule
     * 
     * @param string $string 
     * @param array $delimiters 
     * @param array $exceptions
     * @param string $encoding
	 * @return string
	 */
	public static function ucwords($string, $delimiters = array(" ", "-", ".", "'"), $exceptions = array(), $encoding = "UTF-8"){
        $string = mb_convert_case($string, MB_CASE_TITLE, $encoding);
        foreach ($delimiters as $dlnr => $delimiter) {
            $words = explode($delimiter, $string);
            $newwords = array();
            foreach ($words as $wordnr => $word) {
                if (in_array(mb_strtoupper($word, $encoding), $exceptions)) {
                    // check exceptions list for any words that should be in upper case
                    $word = mb_strtoupper($word, $encoding);
                } elseif (in_array(mb_strtolower($word, $encoding), $exceptions)) {
                    // check exceptions list for any words that should be in upper case
                    $word = mb_strtolower($word, $encoding);
                } elseif (!in_array($word, $exceptions)) {
                    // convert to uppercase (non-utf8 only)
                    $word = ucfirst($word);
                }
                array_push($newwords, $word);
            }
            $string = join($delimiter, $newwords);
       }//foreach
       return $string;
    }


	/**
	 * Permet de mettre la première lettre d'une phrase en majuscule
	 * Fonctionne avec les majuscules
	 *
	 * @param string $string phrase
	 * @param string $encoding
	 * @return string
	 */
	public static function ucfirst($string, $encoding = "UTF-8") {
		$strlen = mb_strlen($string, $encoding);
		$firstChar = mb_substr($string, 0, 1, $encoding);
		$then = mb_substr($string, 1, $strlen - 1, $encoding);
		return mb_strtoupper($firstChar, $encoding).mb_strtolower($then, $encoding);
	}


	/**
	 * Permet de mettre en majuscule la premiere lettre d'un prénoms simple ou composé d'un tirait
	 *
	 * @param string $string phrase
	 * @param string $encoding
	 * @return string
	 */
	public static function capitaliseName($string, $encoding = "UTF-8") {
		// On découpe selon les tirets
		$names = explode('-', $string);

		// On mets une majuscule à chaque élément
		foreach($names as $key => $name):
			$names[$key] = self::ucfirst($name);
		endforeach;

		// On retourne le nom recomposé
		return implode('-', $names);
	}


	/**
	 * Permet de supprimer TOUS les tags html d'un text
	 *
	 * @param string $string
	 * @return string text 
	 */
	static public function rip_tags($string) { 
		// ----- remove HTML TAGs ----- 
		$string = preg_replace ('/<[^>]*>/', ' ', $string); 
		
		// ----- remove control characters ----- 
		$string = str_replace("\r", '', $string);    // --- replace with empty space
		$string = str_replace("\n", ' ', $string);   // --- replace with space
		$string = str_replace("\t", ' ', $string);   // --- replace with space
		
		// ----- remove multiple spaces ----- 
		$string = trim(preg_replace('/ {2,}/', ' ', $string));
		
		return $string;
	} 


	/**
	 * Vérifie la complexité minimal requis par la CNIL
	 * Fonctionne avec les Exception
	 * https://www.cnil.fr/fr/authentification-par-mot-de-passe-les-mesures-de-securite-elementaires
	 * 
	 * @param string $pwd Le mot de passe 
	 * @param integer $length La longueur minimal
	 * @param boolean $letterLow Doit contenir des minuscules
	 * @param boolean $letterUp Doit contenir des majuscules
	 * @param boolean $number Doit contenir des chiffres
	 * @return true
	 */
	static public function checkPassword($pwd, $length = 8, $letterLow = true, $letterUp = true, $number = true) {
		if (strlen($pwd) < $length) 
			throw new \Exception("La longueur du mot de passe est trop court ($length caractère minimums).");		
	
		if ($letterLow && !preg_match("#[a-z]+#", $pwd))
			throw new \Exception("Le mot de passe doit contenir des minuscules.");		

		if ($letterUp && !preg_match("#[A-Z]+#", $pwd))
			throw new \Exception("Le mot de passe doit contenir des majuscules.");		

		if ($number && !preg_match("#[0-9]+#", $pwd))
			throw new \Exception("Le mot de passe doit contenir des chiffres");	
		
		return true;
	}

	/**
	 * Permet de changer une valeur dans un define 
	 *
	 * @param string $psPathFile => Chemin du fichier a modifier
	 * @param string $psKey => Clef a modifier
	 * @param string $psValue => la nouvelle valeur 
	 * @return void
	 */
	public static function change_define($psPathFile, $psKey, $psValue) {
		//on test si le fichier existe
		if( !file_exists($psPathFile) )
			throw new \Exception("Le fichier n'existe pas!");
		
		//On récupérer le fichier source
		$dataSource = file_get_contents($psPathFile);
		
		//On fait le remplacement des data				
		$regex = '/(define\(["\' ]'.$psKey.'["\' ]\, *["\' ])(.*)(["\' ]\)\;)/';
		$data = preg_replace($regex, '$1'.$psValue.'$3', $dataSource);

		//Sauvegarde du fichier, Si pas de match le data est vide, alors on prend la source
		$data = (empty($data) ? $dataSource : $data);
		file_put_contents($psPathFile, $data);
	}

	/**
	 * Permet de formater un string entier pour le csv
	 *
	 * @param string $string Chaine 
	 * @param string $researchChar caractère à rechercher
	 * @param string $replaceChar caractère a de remplacement
	 * @param bool $decodeData Si il faut décoder la chaine
	 * @return string
	 */
	public static function clean_export_csv($string, $researchChar = ';', $replaceChar = ' ', $decodeData = true)
	{
		$string = ($decodeData ? self::decode_data($string) : $string);
		$string = self::rip_tags($string);
		$string = self::normalizeChars($string);
		return str_replace($researchChar, $replaceChar, (string)$string);
	}
	

	/**
	 * Permet de slugifier une string
	 *
	 * @param string $text
	 * @return string
	 */
	public static function slugify($text) 
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\\pL\.\d]+~u', '-', $text);
		// trim
		$text = trim($text, '-');
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		// lowercase
		$text = strtolower($text);
		// remove unwanted characters
		$text = preg_replace('~[^-\.\w]+~', '', $text);
			
		return $text;
	}

	/**
	 * Permet de décoder avec html_entity_decode un array, string, object, ..
	 *
	 * @param mixed $data
	 * @return mixed $data
	 */
	public static function decode_data($data){
		if( $data === null ) :
			return '';
		elseif(is_array($data)) :
			foreach($data as $key => $item):
				$data[$key] = self::decode_data($item);
			endforeach;
		elseif(is_object($data)):
			foreach ($data as $key => $value) :
				$data->$key = html_entity_decode($value, ENT_QUOTES, 'UTF-8');
			endforeach;
		else :
			$data = html_entity_decode($data, ENT_QUOTES, 'UTF-8');
		endif;

		return $data;
	}

	/**
	 * Sécurise la variable utilisateur entrée en paramètre
	 * @author Valentin
	 * @param string $var variable a sécuriser
	 * @return string variable sécurisée
	 */
	public static function secure($var) 
	{
		$return = "";
		if(is_array($var)){
			$return = array();
			foreach($var as $key=>$value)
				$return[self::secure($key)] = self::secure($value);
			
		}else{
			$return = addslashes(htmlspecialchars($var, ENT_QUOTES, "UTF-8"));
		}
		return $return;
	}

	/**
	 * Permet de crypter une string
	 *
	 * @param string $data
	 * @param string $cryptKey
	 * @return string
	 */
	public static function crypt($data, $cryptKey)
	{
		$key = md5($cryptKey);
		$cipher="AES-128-CBC";
		$ivlen = openssl_cipher_iv_length($cipher);
		$iv = openssl_random_pseudo_bytes($ivlen);		
		
		$ciphertext_raw = openssl_encrypt($data, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
		$hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
		
		return base64_encode($iv.$hmac.$ciphertext_raw);
  	}

	/**
	 * Permet de décrypter une string
	 *
	 * @param string $data
	 * @return string
	 */
	public static function decrypt($data, $cryptKey) {
		$key = md5($cryptKey);	
		$c = base64_decode($data);	 
		$cipher="AES-128-CBC";
		$ivlen = openssl_cipher_iv_length($cipher);
		$iv = substr($c, 0, $ivlen);
		$hmac = substr($c, $ivlen, $sha2len=32);
		$ciphertext_raw = substr($c, $ivlen+$sha2len);
		
		return openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
	}	
	  
	/**
	 * 	Permet de tronquer une chaine de caractère
	 *
	 * @param string $psText Chaine de caractère
	 * @param int $maxCharacter Maximum de caractère
	 * @param string $psPrefix
	 * @return string
	 */
	public static function troncate($psText, $maxCharacter, $psPrefix = "...")
	{
		$lenght = 0;

		//On determine si le texte est plus grande que le text a couper
		if( strlen($psText) >= $maxCharacter ):
			$lenght = $maxCharacter;
		else:
			$lenght = strlen($psText);
			$psPrefix = "";
		endif;

		//On retourne la chaine
		return substr($psText, 0, $lenght).$psPrefix;
	}
	
	/**
	 * Retourne True si la string est vide ou null
	 *
	 * @param string $pString
	 * @return bool
	 */
	public static function isEmpty($pString) 
	{
		return !isset($pString) || trim($pString) === '' ? true : false;
	}

	/**
	 * 	Permet de transforme les accents sur les caractère en leurs copie sans accents
	 *
	 * @param string $string Chaine de caractère à modifier
	 * @return string
	 */
	public static function normalizeChars($string) {		
		$normalizeChars = array(
			'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
			'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
			'Ï'=>'I', 'Ñ'=>'N', 'Ń'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 
			'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 
			'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 
			'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ń'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 
			'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',
			'ă'=>'a', 'î'=>'i', 'â'=>'a', 'ș'=>'s', 'ț'=>'t', 'Ă'=>'A', 'Î'=>'I', 'Â'=>'A', 'Ș'=>'S', 'Ț'=>'T',
		);

		return strtr($string, $normalizeChars);
	}


	/**
	 * Détermine si une string est contenue dans une autre
	 *
	 * @param string $needle La string complete
	 * @param string $haystack La recherche
	 * @return bool
	 */
	public static function contains(string $needle, string $haystack) 
	{
		return strpos($haystack, $needle) !== false;
	}


	 /**
	  * Permet de transformer un array en csv 
	  *
	  * @param array $data Le tableau
	  * @param string $delimiter Le délimiteur
	  * @param string $enclosure Caractère échappement
	  * @return string
	  */
	public static function generateCsv($data, $delimiter = ',', $enclosure = '"') 
	{
        $handle = fopen('php://temp', 'r+');
        foreach($data as $line):
            fputcsv($handle, $line, $delimiter, $enclosure);
		endforeach;
		
        rewind($handle);
        $contents = '';
        while( !feof($handle) ):
            $contents .= fread($handle, 8192);
		endwhile;
		
		fclose($handle);
        return $contents;
    }

}