<?php
/**
*	@class \Fabacks\Functions\Tests
*	@description Classe utilitaire pour des tests
*	@author Fabien COLAS
*	@site  dahoo.Fr
*	@gith https://github.com/Fabacks
*	@Copyright Licence CC-by-nc-sa 
*	@Update : 05/02/2020
*	@version 1.1.0
*/
namespace Fabacks\Functions;

Class Tests
{
    
    /**
	 * Permet de déterminer si on est en PHP Commande Line
	 *
	 * @return boolean
	 */
	public static function isPhpCli()
	{
		return (php_sapi_name() === 'cli');
	}

	/**
	 * Définis si la chaine passée en paramètre est un mail
	 *
	 * @param string $mail
	 * @return boolean
	 */
	public static function isMail($mail)
	{
		return (filter_var($mail, FILTER_VALIDATE_EMAIL)) ? true : false;
	}

	/**
	 * Définis si la chaine passée en paramètre est une numéro de téléphone
	 * Mask (33 pour la France): XX XX XX XX XX || +33X XX XX XX XX || 00 330 XX XX XX XX 
 	 *
	 * @param string $phone
	 * @return boolean
	 */
	public static function isPhone($phone)
	{
		// On supprime tout sauf les chiffres
		$phoneFilter = preg_replace('/[^0-9]+/', '', $phone);

		//On contrôle la taille de celle-ci
		if( strlen($phoneFilter) < 10 || strlen($phoneFilter) > 14 ):
			return false;
		endif;
		
		return true;
	}

	/**
	 * Détermine si l'on se trouve sur un serveur windows
	 *
	 * @return boolean
	 */
	public static function isWin()
	{
		return (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN');
	}

	/**
     * Parse une élément est détermine si il est vrais.
	 * Si boolean return ca valeur
	 * Si !boolean return false par défaut ou null si paramètre est spécifier
     *
     * @param boolean|string $val
     * @param boolean $returNull
     * @return boolean
     */
	public static function isTrue($val, $returNull=false)
	{
        $boolval = (is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool)$val);
        return ( $boolval===null && !$returNull ? false : $boolval );
	}

	/**
	 * Test si la chaine passée en paramètre est un numéro de SIREN
	 * @param mixed $pSiren 
	 * @return bool
	 */
	public static function isSiren($pSiren)
	{
		// Le SIREN doit contenir 9 caractères
		if( strlen($pSiren) != 9 )
			return false; 

		// Le SIREN ne doit contenir que des chiffres
		if( !is_numeric($pSiren) )
			return false; 
		
		return true;

		// le numéro est valide si la somme des chiffres est multiple de 10
		return Algorithms::Luhn($pSiren) % 10 == 0 ? true : false;
	}

	/**
	 * Détermine si la chaine passée en paramètre est un numéro de SIRET
	 * 
	 * @param mixed $pSiret 
	 * @return bool 
	 */
	public static function isSiret($pSiret)
	{
		// Le SIRET doit contenir 12 caractères
		if( strlen($pSiret) != 12 )
			return false; 

		// Le SIREN ne doit contenir que des chiffres
		if( !is_numeric($pSiret) )
			return false; 

		return true;

		// le numéro est valide si la somme des chiffres est multiple de 10
		return Algorithms::Luhn($pSiret) % 10 == 0 ? true : false;
	}

    /**
	 * Vérifie si un nombre est multiple d'un autre   
	 * @source : https://phpsources.net/code/php/maths/621_check-si-un-nombre-est-multiple-d-un-autre
	 * 
	 * @param mixed $pNumber le nombre à tester
	 * @param mixed $pMultiple le multiple
	 * @return bool 
	 */
	public static function isMultiple($pNumber, $pMultiple)
	{
		return ($pNumber % $pMultiple == 0) ? true : false;
	}

}